import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { addToDo, editToDo } from "./actions";

const ToDoListForm = ({ edit, isEditing }) => {
  const [fullName, setFullName] = useState("");
  const [task, setTask] = useState("");

  useEffect(() => {
    if (edit?.fullName) setFullName(edit.fullName);
    if (edit?.task) setTask(edit.task);
  }, [edit]);

  const dispatch = useDispatch();

  const handleSubmit = (e) => {
    e.preventDefault();
    if (edit) {
      const id = edit.id;
      dispatch(editToDo({ id, fullName, task }));
      isEditing();
    } else {
      const id = Math.ceil(Math.random() * 10000);
      dispatch(addToDo({ id, fullName, task }));
    }
    setFullName("");
    setTask("");
  };

  return (
    <div className="mini ui left corner labeled input">
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          placeholder="Name"
          value={fullName}
          name="fullName"
          onChange={(e) => setFullName(e.target.value)}
        />
        <input
          type="text"
          placeholder="Task"
          value={task}
          name="task"
          onChange={(e) => setTask(e.target.value)}
        />
        <button> {edit ? "Update" : "Add"}</button>
      </form>
    </div>
  );
};

export default ToDoListForm;
