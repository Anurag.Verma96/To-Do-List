export const addToDo = (toDo) => {
  return {
    type: "ADD_TODO",
    payload: toDo,
  };
};

export const deleteToDo = (id) => {
  return {
    type: "DELETE_TODO",
    payload: {
      id: id,
    },
  };
};

export const editToDo = (toDo) => {
  // console.log(toDo);
  return {
    type: "EDIT_TODO",
    payload: toDo,
  };
};
