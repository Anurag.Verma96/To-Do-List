import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";

import ToDoListForm from "./ToDoListForm";
import { deleteToDo } from "./actions";

const ToDoCard = () => {
  const dispatch = useDispatch();
  const listOfToDo = useSelector((state) => state.toDoReducer.list);

  const [edit, setEdit] = useState({
    id: null,
    fullName: "",
    task: "",
  });

  const isEditing = (value) => {
    setEdit({
      id: null,
      fullName: "",
      task: "",
    });
  };

  if (edit.id) {
    return <ToDoListForm edit={edit} isEditing={isEditing} />;
  }

  if (listOfToDo) {
    return listOfToDo.map((list, index) => (
      <div key={index} className="card">
        <div className="content">
          <i
            className="right floated trash alternate icon"
            onClick={() => {
              dispatch(deleteToDo(list.id));
            }}
          />
          <i
            className="right floated pencil alternate icon"
            onClick={() => {
              setEdit({
                id: list.id,
                fullName: list.fullName,
                task: list.task,
              });
            }}
          />
          <div className="header">
            {list.fullName === "" ? "Anonymous" : `${list.fullName}`}
          </div>
          <div className="description">
            {list.task === "" ? "Take Rest" : `${list.task}`}
          </div>
        </div>
      </div>
    ));
  } else return null;
};

export default ToDoCard;
