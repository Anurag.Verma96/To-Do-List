import { combineReducers } from "redux";

const initialState = {
  list: [],
};

const toDoReducer = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_TODO":
      return {
        ...state,
        list: [...state.list, action.payload],
      };

    case "DELETE_TODO":
      const removedToDo = state.list.filter(
        (toDo) => toDo.id !== action.payload.id
      );

      return {
        ...state,
        list: removedToDo,
      };

    case "EDIT_TODO":
      const editedToDo = state.list.map((toDo) =>
        toDo.id === action.payload.id ? action.payload : toDo
      );
      return {
        ...state,
        editedToDo,
      };

    default:
      return state;
  }
};

const combinedReducer = combineReducers({
  toDoReducer: toDoReducer,
});

export default combinedReducer;
