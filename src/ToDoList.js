import React, { useState } from "react";

import ToDoCard from "./ToDoListCard";
import ToDoListForm from "./ToDoListForm";

const ToDoList = () => {
  // const [toDoList, setToDoList] = useState(
  //   JSON.parse(localStorage.getItem("Todos") || "[]")
  // );

  // Collect Data in localStorage

  // useEffect(() => {
  //   const toDo = JSON.parse(window.localStorage.getItem("Todos"));
  //   if (toDo) {
  //     setToDoList(toDo);
  //   }
  //   return () => {};
  // }, []);

  // Methods for Add, Delete, Edit and storing to localStorage

  // const addToDo = (toDo) => {
  //   const newToDo = [toDo, ...toDoList];
  //   setToDoList(newToDo);
  //   window.localStorage.setItem("Todos", JSON.stringify(newToDo));
  // };

  // const deleteToDo = (toDo, id) => {
  //   const removedToDo = [...toDoList].filter((toDoList) => toDoList.id !== id);
  //   setToDoList(removedToDo);
  //   window.localStorage.setItem("Todos", JSON.stringify(removedToDo));
  // };

  // const updateToDo = (toDoId, newValue) => {
  //   const updatedToDo = toDoList.map((toDo) =>
  //     toDo.id === toDoId ? newValue : toDo
  //   );
  //   setToDoList(updatedToDo);
  //   window.localStorage.setItem("Todos", JSON.stringify(updatedToDo));
  // };

  return (
    <div>
      <h3 className="ui top attached header">
        <div className="ui center aligned icon header">
          <i className="circular users icon" />
          To Do List App
        </div>
        <ToDoListForm
        // For Add
        // onSubmit={addToDo}
        />
      </h3>
      <div className="ui cards">
        <ToDoCard
        // For RenderList/Delete/Edit

        // toDo={toDoList} for
        // deleteToDo={deleteToDo}
        // updateToDo={updateToDo}
        />
      </div>
    </div>
  );
};

export default ToDoList;
