import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore } from "redux";

import ToDoList from "./ToDoList";
import combinedReducer from "./reducers";

const App = () => {
  return <ToDoList />;
};

ReactDOM.render(
  <Provider store={createStore(combinedReducer)}>
    <App />
  </Provider>,
  document.querySelector("#root")
);
